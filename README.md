## Sentinel-1.8.5底层源码改造（持久化到Nacos）

### 如何使用？

#### 1: 修改该源码中的application.properties的部分属性
- server.port   (sentinel控制台的端口)
- nacos.serverAddr (生成的sentinel配置文件所在的nacos的ip地址+端口)
- nacos.group (生成的sentinel配置文件所在的nacos配置中心的分组)
- nacos.namespace (生成的sentinel配置文件所在的nacos配置中心的命名空间)
- nacos.username (nacos帐号)
- nacos.password (nacos密码)

#### 2: 打包项目

**使用如下命令将该源码打成Jar包:**

```bash
mvn clean package
```

#### 3:使用如下命令启动编译后的控制台

- 1:修改-Dserver.port的值为Sentinel的控制台端口
- 2:修改-Dcsp.sentinel.dashboard.server的值为Sentinel控制台的ip:端口号

```bash
java -Dserver.port=8200 -Dcsp.sentinel.dashboard.server=localhost:8200 -Dproject.name=sentinel-dashboard -jar sentinel-dashboard.jar
```

#### 4: SpringCloud应用接入Sentinel

- 1:pom.xml(父pom,依赖控制)
```xml
<properties>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
<!--        SpringBoot版本-->
        <spring.boot.version>2.6.11</spring.boot.version>
<!--        SpringCloud版本-->
        <spring.cloud.version>2021.0.4</spring.cloud.version>
<!--        SpringCloud-Alibaba版本-->
        <spring.cloud.alibaba.version>2021.0.4.0</spring.cloud.alibaba.version>
    </properties>

<!--    依赖管理-->
    <dependencyManagement>
        <dependencies>
<!--            SpringBoot依赖-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring.boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
<!--            SpringCloud依赖-->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring.cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
<!--            SpringCloud-Alibaba依赖-->
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring.cloud.alibaba.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
```

- 2:pom.xml(子pom,也就是你需要接入sentinel的模块)

```xml
<dependencies>
        <!--      SpringCloud Alibaba Nacos注册中心依赖-->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <!-- SpringCloud Alibaba Sentinel流控组件 -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
        </dependency>
<!--        Sentinel的配置持久化到Nacos配置中心-->
        <dependency>
            <groupId>com.alibaba.csp</groupId>
            <artifactId>sentinel-datasource-nacos</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
    </dependencies>
```

- 3:启动类

```java
@SpringBootApplication
@EnableDiscoveryClient
public class SentinelNacosApplication {
    public static void main(String[] args) {
        SpringApplication.run(SentinelNacosApplication.class,args);
    }
}
```

- 4:application.yml(修改下面部分属性)

```yaml
server:
  port: 2303
##################################################################################
# 注意：整合了Nacos注册中心、配置中心需要把有关于spring.xxx的全部配置放到bootstrap.yml文件中，不然会报错！！！！！
##################################################################################
spring:
  application:
    name: sentinel-nacos
  #  main:
  #    allow-bean-definition-overriding: true    #不然多个feign接口连接相同的微服务则会报错
  # SpringCloud Alibaba配置
  cloud:
    # Nacos配置
    nacos:
      # Nacos的ip:端口
      server-addr: 192.168.184.100:7747
    # Sentinel配置
    sentinel:
      # 取消控制台懒加载（默认sentinel是懒加载）
      eager: true
      transport:
        # Sentinel的控制台地址
        dashboard: localhost:8200
        # 指定应用与Sentinel控制台交互的端口，应用本地会起一个该端口占用的HttpServer
        port: 8719
      # 配置Sentinel持久化到Nacos的数据源
      datasource:
        # 配置 “限流” 数据源。只会加载Sentinel上传到Nacos配置中心的“限流”配置文件
        flow-rules:
          # 指定Sentinel持久化到Nacos
          nacos:
            # Nacos的ip+端口
            server-addr: ${spring.cloud.nacos.server-addr}
            # Nacos帐号
            username: nacos
            # Nacos密码
            password: nacos
            # Nacos配置中心上的Sentinel“限流”配置文件的全名（data-id）
            data-id: ${spring.application.name}-flow-rules
            # Nacos配置中心上的Sentinel“限流”配置文件的命名空间
            namespace:
            # Nacos配置中心上的Sentinel“限流”配置文件的分组
            group-id: DEFAULT_GROUP
            # Nacos配置中心上的Sentinel“限流”配置文件的文件类型
            dataType: json
            # Nacos配置中心上的Sentinel“限流”配置文件的规则类型（flow为限流规则）
            ruleType: flow
        # 配置 “降级” 数据源。只会加载Sentinel上传到Nacos配置中心的“降级”配置文件
        degrade‐rules:
          # 指定Sentinel持久化到Nacos
          nacos:
            # Nacos的ip+端口
            server-addr: ${spring.cloud.nacos.server-addr}
            # Nacos帐号
            username: nacos
            # Nacos密码
            password: nacos
            # Nacos配置中心上的Sentinel“降级”配置文件的全名（data-id）
            data-id: ${spring.application.name}‐degrade‐rules
            # Nacos配置中心上的Sentinel“降级”配置文件的命名空间
            namespace:
            # Nacos配置中心上的Sentinel“降级”配置文件的分组
            group-id: DEFAULT_GROUP
            # Nacos配置中心上的Sentinel“降级”配置文件的文件类型
            dataType: json
            # Nacos配置中心上的Sentinel“降级”配置文件的规则类型（degrade为降级规则）
            ruleType: degrade
        # 配置 “参数限流” 数据源。只会加载Sentinel上传到Nacos配置中心的“参数限流”配置文件
        param‐flow‐rules:
          # 指定Sentinel持久化到Nacos
          nacos:
            # Nacos的ip+端口
            server-addr: ${spring.cloud.nacos.server-addr}
            # Nacos帐号
            username: nacos
            # Nacos密码
            password: nacos
            # Nacos配置中心上的Sentinel“参数限流”配置文件的全名（data-id）
            data-id: ${spring.application.name}‐param‐flow‐rules
            # Nacos配置中心上的Sentinel“参数限流”配置文件的命名空间
            namespace:
            # Nacos配置中心上的Sentinel“参数限流”配置文件的分组
            group-id: DEFAULT_GROUP
            # Nacos配置中心上的Sentinel“参数限流”配置文件的文件类型
            dataType: json
            # Nacos配置中心上的Sentinel“参数限流”配置文件的规则类型（param_flow为参数限流规则）
            ruleType: param_flow
        # 配置 “授权规则” 数据源。只会加载Sentinel上传到Nacos配置中心的“授权规则”配置文件
        authority‐rules:
          # 指定Sentinel持久化到Nacos
          nacos:
            # Nacos的ip+端口
            server-addr: ${spring.cloud.nacos.server-addr}
            # Nacos帐号
            username: nacos
            # Nacos密码
            password: nacos
            # Nacos配置中心上的Sentinel“授权规则”配置文件的全名（data-id）
            data-id: ${spring.application.name}‐authority‐rules
            # Nacos配置中心上的Sentinel“授权规则”配置文件的命名空间
            namespace:
            # Nacos配置中心上的Sentinel“授权规则”配置文件的分组
            group-id: DEFAULT_GROUP
            # Nacos配置中心上的Sentinel“授权规则”配置文件的文件类型
            dataType: json
            # Nacos配置中心上的Sentinel“授权规则”配置文件的规则类型（authority为授权规则）
            ruleType: authority
        # 配置 “系统规则” 数据源。只会加载Sentinel上传到Nacos配置中心的“系统规则”配置文件
        system‐rules:
          # 指定Sentinel持久化到Nacos
          nacos:
            # Nacos的ip+端口
            server-addr: ${spring.cloud.nacos.server-addr}
            # Nacos帐号
            username: nacos
            # Nacos密码
            password: nacos
            # Nacos配置中心上的Sentinel“系统规则”配置文件的全名（data-id）
            data-id: ${spring.application.name}‐system‐rules
            # Nacos配置中心上的Sentinel“系统规则”配置文件的命名空间
            namespace:
            # Nacos配置中心上的Sentinel“系统规则”配置文件的分组
            group-id: DEFAULT_GROUP
            # Nacos配置中心上的Sentinel“系统规则”配置文件的文件类型
            dataType: json
            # Nacos配置中心上的Sentinel“系统规则”配置文件的规则类型（system为系统规则）
            ruleType: system


management:
  endpoints:
    web:
      exposure:
        include: "*"

```


#### 5:判断是否接入Sentinel成功

- **1:SpringCloud Alibaba项目整合Sentinel(省略)**
- **2:访问整合后的SpringCloud Alibaba项目其中某个接口（该接口要加@SentinelResource注解）。**
- **3:打开Sentinel控制台查看刚刚的请求是否能被监控。**



